import React, { useState } from 'react';
import styled from 'styled-components'
import Modal from '../components/Modal'

const Button = styled.button`
  min-width: 100px;
  padding: 16px 32px;
  border-radius: 4px;
  border: none;
  background: #141414;
  color: #fff;
  font-size: 24px;
  cursor: pointer;
`;

const SimonSaysScreen = () => {
  const [showModal, setShowModal] = useState(false)

  const openModal = () => {
    setShowModal(prev => !prev)
  }

  return (
    <div className="container">
      <Button onClick={openModal}>I'm a modal</Button>
      <Modal showModal={showModal} setShowModal={setShowModal} />
    </div>
  );
}

export default SimonSaysScreen;

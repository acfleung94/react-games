import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'
import HigherOrLowerFooter from '../components/higherOrLower/higherOrLowerFooter'
import HigherOrLowerScoreTracker from '../components/higherOrLower/HigherOrLowerScoreTracker'
import HigherOrLowerCard from '../components/higherOrLower/HigherOrLowerCard'
import Modal from '../components/Modal'

const HigherOrLowerScreen = () => {
  const [score, setScore] = useState(0)
  const [highScore, setHighScore] = useState(0)

  const [lastNumber, setLastNumber] = useState('')
  const [lastSuite, setLastSuite] = useState('')

  const [number, setNumber] = useState('')
  const [suite, setSuite] = useState('');

  const [nextNumber, setNextNumber] = useState('')
  const [nextSuite, setNextSuite] = useState('');

  const [showModal, setShowModal] = useState(false)

  useEffect(() => {
    setCardValues();
  }, [])

  const setCardValues = () => {
    const newNumberValue = Math.round(Math.random() * 12) +1
    setNumber(newNumberValue)

    const newNextNumberValue = Math.round(Math.random() * 11) +1
    setNextNumber(newNextNumberValue)

    const suiteArray = [0,1,2,3]

    const newSuiteValue = Math.round( Math.random()*(suiteArray.length - 1) )
    setSuite(newSuiteValue)

    let newNextSuiteValue = ''
    if (newNumberValue === newNextNumberValue) {
      const filteredSuite = suiteArray.filter(item => item !== newSuiteValue)
      newNextSuiteValue = filteredSuite[Math.round(Math.random()*(filteredSuite.length - 1))]
    } else {
      newNextSuiteValue = Math.round( Math.random()*(suiteArray.length - 1) )
    }
    setNextSuite(newNextSuiteValue)
  }

  const compareCard = (value) => {
    comparisonLogic(value)
    setCardValues()
  }

  const comparisonLogic = (value) => {
    if ( value === "Lower") {
      nextNumber < number && correct()
      nextNumber > number && incorrect()
    }

    if ( value === "Higher" ) {
      nextNumber > number && correct()
      nextNumber < number && incorrect()
    }

    nextNumber === number && compareSuite(value)
  }

  const compareSuite = (value) => {
    if ( value === "Lower") {
      nextSuite < suite && correct()
      nextSuite > suite && incorrect()
    }
    
    if ( value === "Higher" ) {
      nextSuite > suite && correct()
      nextSuite < suite && incorrect()
    }
  }

  const correct = () => {
    setScore(score + 1)
    setLastNumber(number)
    setLastSuite(suite)
  }

  const incorrect = () => {
    setLastNumber('')
    setLastSuite('')
    setShowModal(true)
  }

  return (
    <div className="higher-or-lower-container">
      <div className="higher-or-lower-header row">
        <Link to="/">
          <div className="back-arrow">
            <h3>{"<"}</h3>
          </div>
        </Link>
        <div className="title">
          <h3>Higher or Lower</h3>
        </div>
        
      </div>

      <div className="higher-or-lower-score-tracker">
        <HigherOrLowerScoreTracker 
          highScore={highScore} score={score} 
          lastNumber={lastNumber} lastSuite={lastSuite} 
        />
      </div>

      <div className="higher-or-lower-card-deck">
        <HigherOrLowerCard 
          number={number} suite={suite}
        />
      </div>

      <div className="higher-or-lower-footer">
        <HigherOrLowerFooter compare={compareCard} />
      </div>

      <Modal showModal={showModal} setShowModal={setShowModal} 
        score={score} highScore={highScore}
        setScore={setScore} setHighScore={setHighScore}
      />
    </div>
  )
}

export default HigherOrLowerScreen
import { Link } from 'react-router-dom'
import Header from '../components/gameScreen/Header'
import GameCard from '../components/gameScreen/GameCard'
import Footer from '../components/gameScreen/Footer'

const GamesScreen = () => {
  return (
    <div className="container">
      <div className="game-screen-header">
        <Header />
      </div>
      
      <div className="game-cards-container">
        <div className="game-card higher-or-lower-card">
          <Link to="/higherOrLower">
            <GameCard name={'Higher Or Lower'}/>
          </Link>          
        </div>

        <div className="game-card card-spacer simon-says-card">
          <Link to="/simonSays">
            <GameCard name={'Simon Says'}/>
          </Link>
        </div>

        <div className="game-card card-spacer memory-card">
          <GameCard name={'Memory'}/>
        </div>

        <div className="game-card countdown-letters-card">
          <GameCard name={'Countdown Letters'}/>
        </div>
      </div>

      <div className="game-screen-footer">
        <Footer />
      </div>
    </div>
  );
}

export default GamesScreen;

import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'

import GamesScreen from './screens/GamesScreen'
import HigherOrLowerScreen from './screens/HigherOrLowerScreen'
import SimonSaysScreen from './screens/SimonSaysScreen'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Redirect to="/index" />
        </Route>
        <Route path="/index" component={GamesScreen} />
        <Route path="/higherOrLower" component={HigherOrLowerScreen} />
        <Route path="/simonSays" component={SimonSaysScreen} />
      </Switch>
    </Router>
  );
}

export default App;

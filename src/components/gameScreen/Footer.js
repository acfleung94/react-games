import { FaDiceD20, FaFileAlt, FaAward, FaHeartbeat } from 'react-icons/fa'

const Footer = () => {
  return (
    <footer className="row">      
      <div className="game-screen-footer-column">
        <FaDiceD20 style={{ fontSize: 32 }}/>GAMES
      </div>
      <div className="game-screen-footer-column">
        <FaFileAlt style={{ fontSize: 32 }}/>STATS
      </div>
      <div className="game-screen-footer-column">
        <FaAward style={{ fontSize: 32 }}/>AWARDS
      </div>
      <div className="game-screen-footer-column">
        <FaHeartbeat style={{ fontSize: 32 }}/>PROFILE
      </div>
    </footer>
  )
}

export default Footer

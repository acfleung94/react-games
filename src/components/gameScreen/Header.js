import { FaDice } from 'react-icons/fa'

const Header = () => {
  return (
    <header className="row">      
      <FaDice style={{ fontSize: 50 }}/>
      <h2>Yozu Games</h2>
    </header>
  )
}

export default Header

const GameCard = ({ name }) => {
  return (
    <div className="row">      
      <h2>{name}</h2>
    </div>
  )
}

export default GameCard

import { ImClubs, ImHeart, ImSpades } from 'react-icons/im'
import { BsFillDiamondFill } from 'react-icons/bs'

const HigherOrLowerScoreTracker = ({ highScore, score, lastNumber, lastSuite }) => {
  let displaySuite = ''
  switch (lastSuite) {
    case 0:
      displaySuite = <BsFillDiamondFill />
      break;
    case 1:
      displaySuite = <ImClubs />
      break;
    case 2:
      displaySuite = <ImHeart />
      break;
    case 3:
      displaySuite = <ImSpades />
      break;
    default:
      displaySuite = ""
  };

  return (
    <div className="higher-or-lower-score-row">

      <div className="higher-or-lower-score-column">
        <div className="higher-or-lower-column_1a">
          <p>High Score</p>
        </div>
        <div className="higher-or-lower-column_2">
          <p>{highScore}</p>
        </div>
      </div>

      <div className="higher-or-lower-score-column">
        <div className="higher-or-lower-column_1b">
          <p>Score</p>
        </div>
        <div className="higher-or-lower-column_2">
          <p>{score}</p>
        </div>
      </div>

      <div className="higher-or-lower-score-column">
        <div className="higher-or-lower-column_1c">
          <p>Last Card</p>
        </div>
        <div className="higher-or-lower-column_2b">
          <p>{lastNumber} {displaySuite}</p>
        </div>
      </div>
      
    </div>
  )
}

export default HigherOrLowerScoreTracker
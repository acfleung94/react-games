import { ImClubs, ImHeart, ImSpades } from 'react-icons/im'
import { BsFillDiamondFill } from 'react-icons/bs'

const HigherOrLowerCard = ({ number, suite }) => {
  let displaySuite = ''
  switch (suite) {
    case 0:
      displaySuite = <BsFillDiamondFill />
      break;
    case 1:
      displaySuite = <ImClubs />
      break;
    case 2:
      displaySuite = <ImHeart />
      break;
    case 3:
      displaySuite = <ImSpades />
      break;
    default:
      displaySuite = <BsFillDiamondFill />
  };

  let displayNumber = ''
  switch (number) {
    case 1:
      displayNumber = "A"
      break;
    case 11:
      displayNumber = "J"
      break;
    case 12:
      displayNumber = "Q"
      break;
    case 13:
      displayNumber = "K"
      break;
    default:
      displayNumber = number
  };

  return (
    <div>
      <div className="top-card-suite">
        {displaySuite}
      </div>

      <div className="card-number">
        {displayNumber}
      </div>

      <div className="bottom-card-suite">
        {displaySuite}
      </div>
    </div>

  )
}

export default HigherOrLowerCard
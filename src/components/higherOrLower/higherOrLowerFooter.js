const HigherOrLowerFooter = ({ compare }) => {
  return (
    <footer className="row">      
      <button 
        className="higher-or-lower-footer-column lower-tab"
        onClick={() => compare("Lower")}
      >
        <p>Lower</p>
      </button>
      <button 
        className="higher-or-lower-footer-column higher-tab"
        onClick={() => compare("Higher")}
      >
        <p>Higher</p>
      </button>
    </footer>
  )
}

export default HigherOrLowerFooter

import styled from 'styled-components'

const Background = styled.div`
  width: 398px;
  height: 748px;
  background: rgba(200, 200, 200, 0.8);
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: -30px;
  margin-top: -628px;
`

const ModalWrapper = styled.div`
  width: 100%;
  height: 600px;
  background: #fff;
  color: #000;
  position: relative;
  z-index: 10;
  margin-top: 148px;
`

const Modal = ({ showModal, setShowModal, score, highScore, setScore, setHighScore }) => {
  return (
    <>
      {showModal ? (
        <Background>
          <ModalWrapper showModal={showModal}>
            <div className='modal'>
              <div className='modal-title'>
                {score > highScore ? <h2>Congratulations!</h2> : <h2>Sorry, you've lost!</h2> }
              </div>

              {score > highScore ? <p>New highest score</p> : <p>Bad luck this time</p> }
              
              <div className='score-indicator'>
                <h1>{score}</h1>
              </div>
            </div>

            <div className='modal-footer-tab'>
              <footer className='modal-footer-content'>
                <button 
                  className='modal-footer-button'
                  onClick={() => {
                    setScore(0)
                    setHighScore(score)
                    setShowModal(false)
                  }}  
                >
                  <h2>Close</h2>
                </button>
              </footer>
            </div>

          </ModalWrapper>
        </Background>
      ) : null }
    </>
  )
}

export default Modal;
import { render, screen } from '@testing-library/react';
import GamesScreen from '../../screens/GamesScreen';

test('all text are rendered for gameScreen', () => {
  render(<GamesScreen />);

  const linkElement = screen.getByText(/Yozu Games/i);
  expect(linkElement).toBeInTheDocument();

  const higherOrLowerText = screen.getByText(/Higher Or Lower/i);
  expect(higherOrLowerText).toBeInTheDocument();

  const simonSaysText = screen.getByText(/Simon Says/i);
  expect(simonSaysText).toBeInTheDocument();

  const memoryText = screen.getByText(/Memory/i);
  expect(memoryText).toBeInTheDocument();

  const countdownLetters = screen.getByText(/Countdown Letters/i);
  expect(countdownLetters).toBeInTheDocument();
});
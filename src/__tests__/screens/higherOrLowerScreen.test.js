import { render, fireEvent, screen, getByText } from '@testing-library/react'
import HigherOrLowerScreen from '../../screens/HigherOrLowerScreen'

test('all text are rendered for higherOrLowerScreen', async () => {
  render(<HigherOrLowerScreen />)

  const titleText = screen.getByText(/Higher or Lower/i);
  expect(titleText).toBeInTheDocument();

  const highScoreText = screen.getByText(/High Score/i);
  expect(highScoreText).toBeInTheDocument();

  const lastCardText = screen.getByText(/Last Card/i);
  expect(lastCardText).toBeInTheDocument();
})